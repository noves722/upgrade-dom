//1.1 Usa querySelector para mostrar por consola el botón con la clase .showme
const button$$ =document.querySelector('.showme');
console.log(button$$);
//1.2 Usa querySelector para mostrar por consola el h1 con el id #pillado
const h1$$ = document.querySelector('#pillado');
console.log(h1$$)
//1.3 Usa querySelector para mostrar por consola todos los p
const p$$ = document.querySelectorAll('p');
console.log(p$$);
//1.4 Usa querySelector para mostrar por consola todos los elementos con la clase.pokemon
const pokemon$$ = document.querySelectorAll('.pokemon');
console.log(pokemon$$);
//1.5 Usa querySelector para mostrar por consola todos los elementos con el atributo 
//data-function="testMe".
const data$$ = document.querySelectorAll('[data-function="testMe"]');
console.log(data$$);
//1.6 Usa querySelector para mostrar por consola el 3 personaje con el atributo 
//data-function="testMe".
console.log(data$$[3]);



/*ITERACION 2*/

//2.1 Inserta dinamicamente en un html un div vacio con javascript.
const divD = document.createElement('div');
divD.innerText='hola';
document.body.appendChild(divD);
//console.log(button$$);
//2.2 Inserta dinamicamente en un html un div que contenga una p con javascript.
const body$$ = document.querySelector('body');
body$$.innerHTML+='<div><p>Añadido dinamicamente</p></div>'
const divDD = document.createElement('div');
const pDD = document.createElement('p');
pDD.innerText='añadido dinamicamente de otra forma'
divDD.appendChild(pDD);
document.body.appendChild(divDD);
//2.3 Inserta dinamicamente en un html un div que contenga 6 p utilizando un loop con javascript.
var newDiv = document.querySelector('body');
for (let i = 0; i < 6; i++) {
    
    newDiv.innerHTML+='<p>"soy el P ", i </p>';
       
};

const divP=document.createElement('div');
for (let i = 0; i < 6; i++) {
          divP.innerHTML+='<p>'+'soy el P '+ i+'</p>';
}
document.body.appendChild(divP);


//2.4 Inserta dinamicamente con javascript en un html una p con el texto 'Soy dinámico!'.

//2.5 Inserta en el h2 con la clase .fn-insert-here el texto 'Wubba Lubba dub dub'.
const h2$$ = document.querySelector('.fn-insert-here');
h2$$.innerText = 'Wubba Lubba dub dub';

//2.6 Basandote en el siguiente array crea una lista ul > li con los textos del array.
const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter'];
const newlist = document.createElement('ul');
for (let i = 0; i < apps.length; i++) {    
    newlist.innerHTML+='<li>'+apps[i]+'</li>';       
};
document.body.appendChild(newlist);

//2.7 Elimina todos los nodos que tengan la clase .fn-remove-me
const allFN = document.querySelectorAll('.fn-remove-me');
console.log(allFN);
for (let i = 0; i < allFN.length; i++) {    
    allFN[i].remove();       
};

//2.8 Inserta una p con el texto 'Voy en medio!' entre los dos div. 
	//Recuerda que no solo puedes insertar elementos con .appendChild.
    const pEntre = document.createElement('p');
    pEntre.innerText = 'Voy en medio!';
    const divs = document.querySelectorAll('div');
    
    document.body.insertBefore(pEntre, divs[1]);
//2.9 Inserta p con el texto 'Voy dentro!', dentro de todos los div con la clase .fn-insert-here
    //const pDentro = document.createElement('p');
    //pDentro.innerText = 'Voy en dentro!';
    let divss = document.querySelectorAll('.fn-insert-here');
    console.log(divss);
for (let i = 0; i < divss.length; i++) {    
    divss[i].innerHTML+='<p>"voy dentro"</p>'       
};